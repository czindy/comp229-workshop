# Task 0

Clone this repository (well done!)

# Task 1

Take a look at the two repositories:

  * (A) https://bitbucket.org/altmattr/personalised-correlation/src/master/
  * (B) https://github.com/Whiley/WhileyCompiler

And answer the following questions about them:

  * These repositories are at two different websites - github and bitbucket 
		- what are these sites?  
			> Github and Bitbucket are two of the largest web-based hosting services for source code and development projects. 
			> They take different approaches to private vs. public repositories that affect the ease of collaboration and the risk of data exposure.
		- What service do they provide? 
			> It has made easier to manage large projects.
			> It helps you to avoid overwriting the team�s advances and work.
			> With git, you just pull the entire code and history to your system. It's much simpler and much more lightweight.
		- Which is better?
			> The choice may come down to the structures of your projects. If you have many private projects and small numbers 
			of users per project, Bitbucket may be a cheaper option because of its per-repo pricing. If you have large teams 
			collaborating on just a few projects, Github may be the better option.
		
  
  * Who made the last commit to repository A?
		- Jharyd Marhkain
		
  * Who made the first commit to repository A?
		-  Jon Mountjoy
		
  * Who made the first and last commits to repository B?
		- First: DavePearce 
		- Last: DavePearce
		
  * Are either/both of these projects active at the moment? If not, what do you think happened?
		- Both project are still active because at the Commit section, both project last update are recent.
		
  * Which file in each project has had the most activity?
		- 

# Task 2

Setup a new IntelliJ project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~

# Task 3

Draw a 20 by 20 grid on a 1280x720 window. Have the grid take up the 720x720 square on the left of the window.  
Each cell in the grid should be 35 pixels high and wide and the grid should be drawn 10 pixels off the top and left 
borders of the screen.  To do this, you should use the `Graphics` class from the Java libraries.  
Be sure to consult the tips page for this task (it is a link in iLearn).  
Without it, you will be very confused.

# *** Task 4

Create a 2D array to represent the grid and connect the drawn grid to the array in some way.

# Task 5

Modify your program so that mousing over a cell will "highlight" it.  Highlighted cells should be drawn in grey.

# Task 6

Ensure your program, if it does not already, has a `Cell` class and that your grid array is an array of `Cell` objects.  
It should still display as before.  What are reasonable methods and fields for the `Cell` class?  
Now create a `Grid` class to subsume your 2D array of `Cell`s.  What fields and methods should this class have?



